[更新记录](https://gitee.com/raoyutian/PaddleOCR.Onnx/blob/master/doc/README_update.md)

#### 如果对你有用或者喜欢，那就给颗星赞，点个赞。谢谢！

## 一、介绍

本项目使用ONNX模型，速度更快。本项目同时支持X64和X86的CPU上上用。
本项目是一个基于[PaddleOCR](https://github.com/paddlepaddle/PaddleOCR)的C++代码修改并基于Microsoft.ML封装的.NET的工具类库。包含文本识别、文本检测、基于文本检测结果的统计分析的表格识别功能，同时针对小图识别不准的情况下，做了优化，提高识别准确率。包含总模型仅8.6M的超轻量级中文OCR，单模型支持中英文数字组合识别、竖排文本识别、长文本识别。同时支持多种文本检测。
项目封装极其简化，实际调用仅几行代码，极大的方便了中下游开发者的使用和降低了PaddleOCR的使用入门级别，同时提供不同的.NET框架使用，方便各个行业应用开发与部署。Nuget包即装即用，可以离线部署，不需要网络就可以识别的高精度中英文OCR。  


本项目依赖VC++2017,如果当前计算机没有安装，请安装VC++2017,或者vc++2015-2019或者vc++2015-2022。

本项目目前支持以下NET框架：

```
net461;net462;net47;net471;net48;net481;
netstandard2.0;netcoreapp3.1;
net5.0;net6.0;net7.0;

```
代码推荐使用VS2022版本编译。

OCR识别模型库支持官方所有的模型，也支持自己训练的模型。

本项目部署自带的一种轻量版8.6M模型库、服务器版模型库（更准确，需要自行下载），可以自行更改模型库适用实际需求。

[PaddleOCR模型下载地址](https://gitee.com/paddlepaddle/PaddleOCR/blob/dygraph/doc/doc_ch/models_list.md)

 **模型需要转成ONNX格式采用被本项目所使用。** 
[PaddleOCR模型转换](https://gitee.com/PaddlePaddle/PaddleOCR/blob/dygraph/deploy/paddle2onnx/readme.md)

如果需要修改成服务器版模型库，参考代码如下：(假设服务器版模型库在运行目录的文件夹inferenceserver下)

```
 //自带轻量版中英文模型
 // OCRModelConfig config = null;
 //服务器中英文模型
 //OCRModelConfig config = new OCRModelConfig();
 //string root = Environment.CurrentDirectory;
 //string modelPathroot = root + @"\inferenceserver";
 //config.det_infer = modelPathroot + @"\ch_ppocr_server_v2.0_det_infer.onnx";
 //config.cls_infer = modelPathroot + @"\ch_ppocr_mobile_v2.0_cls_infer.onnx";
 //config.rec_infer = modelPathroot + @"\ch_ppocr_server_v2.0_rec_infer.onnx";
 //config.keys = modelPathroot + @"\ppocr_keys.txt";

 //英文和数字模型
 OCRModelConfig config = new OCRModelConfig();
 string root = Environment.CurrentDirectory;
 string modelPathroot = root + @"\en";
 config.det_infer = modelPathroot + @"\ch_PP-OCRv2_det_infer";
 config.cls_infer = modelPathroot + @"\ch_ppocr_mobile_v2.0_cls_infer.onnx";
 config.rec_infer = modelPathroot + @"\en_number_mobile_v2.0_rec_infer.onnx";
 config.keys = modelPathroot + @"\en_dict.txt";

 PaddleOCREngine engine = new PaddleOCREngine(config, oCRParameter);

```

## 二、.NET使用示例

```
  OpenFileDialog ofd = new OpenFileDialog();
  ofd.Filter = "*.*|*.bmp;*.jpg;*.jpeg;*.tiff;*.tiff;*.png";
  if (ofd.ShowDialog() != DialogResult.OK) return;
  var imagebyte = File.ReadAllBytes(ofd.FileName);
  Bitmap bitmap = new Bitmap(new MemoryStream(imagebyte));
  OCRModelConfig config = null;
  OCRParameter oCRParameter = new  OCRParameter ();

  OCRResult ocrResult = new OCRResult();

  //建议程序全局初始化一次即可，不必每次识别都初始化，容易报错。     
  PaddleOCREngine engine = new PaddleOCREngine(config, oCRParameter);
   {
    ocrResult = engine.DetectText(bitmap );
   }
 if (ocrResult != null)
 {
    MessageBox.Show(ocrResult.Text,"识别结果");
 }

```



### QQ交流群：318860399，818391797 ，有问题可以加QQ群咨询。

### 微信公众号：明月心技术学堂。

